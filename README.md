# Cisco Interactive Survey Devnet Demo
This demo uses AAP2.2 to demonstrate an usecase to glean the status of availble ports and assign an IP address to a selected port. A survey is used to prompt the network operator for information to complete the afore mentioned job-template/playbook tasks. Initially the operator selects the device from a job-template survey which in turn launches playbook/roles to respond with the availble "shut" interfaces. This list of interfaces will update a second job-template survey. The network operator selects the available port to add an IP address to the interface. As a consequence, the associated playbook then pushes the appropriate configuration to the selected device.

## Git Repository
Point your own AAP2 project to this GitLab repo to provide the necessary files to run the demo.
Since this Repo is public, no SCM credentials are required to update the Ansible Controller's project. 

## Install AAP 2.x 

### (Free 60 Day Trial)
http://ansible.com/license


### Developer License
Another option is to sign up for a free developer license.
https://developers.redhat.com/

### Install Guide
https://access.redhat.com/documentation/en-us/red_hat_ansible_automation_platform/2.2/html/red_hat_ansible_automation_platform_installation_guide


Only the AAP controller install is required to run this demo. Please allocate a minimum of 16gig ram and 4 vcpus for laptops or server VM/Rhel OS installs. Please note that a VPN is needed to connect to Cisco Devnet sandbox.


## Devnet ACI Sandbox
This demo uses the Devnet Reserved Sandbox "Cisco Modeling Labs Enterprise. You are provided free access to this Devnet Sandbox by using one of the approved login methods. Please note the reservation can be adjusted from the default 4 hours to 2 days when scheduled.

 https://devnetsandbox.cisco.com/RM/Diagram/Index/45100600-b413-4471-b28e-b014eb824555?diagramType=Topology

 Please Note the default CML toplogy "Small NXOS/IOSXE Network" topology will work for this demo. The two Nexus 9kv switches are used in particular. 

 * dist-sw01 10.10.20.177  cisco/cisco
 * dist-sw02 10.10.20.178  cisco/cisco 

### VPN
This demo requires a VPN from your laptop/VM or standalone VM to the Devnet Sandbox. Cisco emails the credentials. 

#### Download
https://developer.cisco.com/site/devnet/sandbox/anyconnect/

#### VPN Install
https://devnetsandbox.cisco.com/Docs/VPN_Access/AnyConnect_Installation_Guide.pdf

### controller.yml
This playbook builds the Ansible Automation Platform Controller "Tower" installion this playbook from your laptop to configure your AAP2.x as code. Clone this repo to run this playbook locally with either ansible-playbook or ansible-navigator. The playbook also has a dependency for the ansible.controller collection. 
https://console.redhat.com/ansible/automation-hub/ansible/tower/


~~~
git clone https://gitlab.com/tdubiel1/cisco-interactive-devnet.git
~~~

## Run the demo in the following order

### 1-Prepare-Env
This job template maps to the prepare_devnet_env.yml playbook to set the intial confguration for the default CML toplogy "Small NXOS/IOSXE Network".

### 2-PreFlight-Get-Available-Interfaces
This job template maps to the playbooks/interface.yml playbook to glean the available shut interfaces from the selected device and update the survey input options for the next step.

### 3-Add-IP-Address
This job template maps to the playbooks/address.yml playbook to configure the appropriate interface with an Ip address.



